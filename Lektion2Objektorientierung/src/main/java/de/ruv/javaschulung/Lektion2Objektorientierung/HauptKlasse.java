package de.ruv.javaschulung.Lektion2Objektorientierung;

/**
 * Hello world!
 *
 */
public class HauptKlasse {
	
	public static DrachenFarm[] farmen = new DrachenFarm[2];

	public static void aufbauFarmen() {
		DrachenZuechter peter = new DrachenZuechter("Hans", "Peter", new Datum(3, 5, 1990), 42);

		farmen[0] = new DrachenFarm("Sonnenblumenwiese", peter, 5);
		farmen[0].aufnahme(new Drache("Herbert", new Datum(12, 8, 1423)));
		farmen[0].aufnahme(new Drache("Fraubert", new Datum(24, 2, 1377)));
		farmen[0].aufnahme(new Drache("Sonbert", new Datum(5, 4, 1546), farmen[0].drachen[1], farmen[0].drachen[0]));
		farmen[0].aufnahme(new Drache("Tochterbert", new Datum(2, 12, 1611), farmen[0].drachen[1], farmen[0].drachen[0]));
		farmen[0].aufnahme(new Drache("Kleinbert", new Datum(27, 6, 2001), farmen[0].drachen[1], farmen[0].drachen[0]));

		DrachenZuechter weiss = new DrachenZuechter("Walter", "Weiss", new Datum(24, 12, 1983), 4);

		farmen[1] = new DrachenFarm("New Mexico", weiss, 3);
		farmen[1].aufnahme(new Drache("Fritz", new Datum(9, 7, 1033)));
		farmen[1].aufnahme(new Drache("Blitz", new Datum(3, 3, 3333)));
		farmen[1].aufnahme(new Drache("Witz", new Datum(7, 4, 1844), farmen[1].drachen[1], farmen[1].drachen[0]));
	}

	public static String datumZuString(Datum datum) {
		return "" + String.format("%02d", datum.tag) + "." + String.format("%02d", datum.monat) + "." + datum.jahr;
	}

	public static void ausgabeZuechter(DrachenZuechter zuechter) {
		System.out.println("\tName: " + zuechter.vorName + " " + zuechter.nachName);
		System.out.println("\tGeburtsdatum: " + datumZuString(zuechter.geburtsDatum));
		System.out.println("\tLebensversicherungsnummer: " + zuechter.lebensVSN);
	}

	public static void ausgabeDrache(Drache drache) {
		System.out.println("Name: " + drache.name);
		System.out.println("Geburtsdatum: " + datumZuString(drache.geburtsDatum));

		if (drache.mutter != null && drache.vater != null) {
			System.out.println("Eltern: " + drache.mutter.name + " & " + drache.vater.name);
		}
	}

	public static void ausgabeFarmen() {
		for (DrachenFarm farm : farmen) {
			System.out.println("==================================================================");
			System.out.println("Farm: " + farm.name);
			System.out.println("Züchter:");
			ausgabeZuechter(farm.zuechter);
			System.out.println("==================================================================");
			for (Drache drache : farm.drachen) {
				ausgabeDrache(drache);
				System.out.println("------------------------------------------------------------------");
			}
		}
	}

	public static void main(String[] args) {
		aufbauFarmen();
		ausgabeFarmen();
	}
	
}
