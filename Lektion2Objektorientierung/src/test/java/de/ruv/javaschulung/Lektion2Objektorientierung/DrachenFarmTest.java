package de.ruv.javaschulung.Lektion2Objektorientierung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DrachenFarmTest extends TestCase {
	
	public DrachenFarmTest(String testName) {
		super(testName);
	}
	
	public static Test suite() {
		return new TestSuite(DrachenFarmTest.class);
	}
	
	public void testDrachenFarmKonstruktion() {
		
		DrachenZuechter dz = new DrachenZuechter("vor", "nach", new Datum(1900), 42);
		
		DrachenFarm df1 = new DrachenFarm("df1", dz, 10);
		assertEquals("df1", df1.name);
		assertEquals(dz, df1.zuechter);
		assertEquals(10, df1.drachen.length);
		assertEquals(0, df1.anzahl);
		
		DrachenFarm df2 = new DrachenFarm("df2", dz, 4);
		assertEquals("df2", df2.name);
		assertEquals(dz, df2.zuechter);
		assertEquals(4, df2.drachen.length);
		assertEquals(0, df2.anzahl);
		
		DrachenFarm df3 = new DrachenFarm("df3", dz, 42);
		assertEquals("df3", df3.name);
		assertEquals(dz, df3.zuechter);
		assertEquals(42, df3.drachen.length);
		assertEquals(0, df3.anzahl);
		
	}
	
	public void testDrachenFarmAufnahme() {
		
		int max = 11;
		
		DrachenFarm df = new DrachenFarm("farm",
								new DrachenZuechter("vor", "nach", new Datum(1111), 42)
								, max);
		
		
		for (int i = 0; i < max; i++) {
			Drache d = new Drache("drache", new Datum(1));
			
			assertEquals(df.anzahl, i);
			assertNull(df.drachen[i]);
			
			df.aufnahme(d);
			
			assertEquals(df.anzahl, i + 1);
			assertNotNull(df.drachen[i]);
			assertEquals(d, df.drachen[i]);
		}
		
		Drache d = new Drache("dragon", new Datum(2));
		
		assertEquals(max, df.anzahl);
		
		assertNotSame(d, df.drachen[max-1]);
		
		df.aufnahme(d);
		
		assertEquals(max, df.anzahl);
		
		assertNotSame(d, df.drachen[max-1]);
		
	}
	
}
