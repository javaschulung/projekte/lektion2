package de.ruv.javaschulung.Lektion2Objektorientierung;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DracheTest extends TestCase {

	public DracheTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(DracheTest.class);
	}

	public void testDracheKonstruktion() {
		
		Datum datum = new Datum(2000);
		
		Drache d1 = new Drache("d1", datum);
		assertEquals("d1", d1.name);
		assertEquals(datum, d1.geburtsDatum);
		assertNull(d1.mutter);
		assertNull(d1.vater);
		
		Drache d2 = new Drache("d2", datum);
		assertEquals("d2", d2.name);
		assertEquals(datum, d2.geburtsDatum);
		assertNull(d2.mutter);
		assertNull(d2.vater);
		
		Drache d3 = new Drache("d3", datum, d1, d2);
		assertEquals("d3", d3.name);
		assertEquals(datum, d3.geburtsDatum);
		assertNotNull(d3.mutter);
		assertEquals(d1, d3.mutter);
		assertNotNull(d3.vater);
		assertEquals(d2, d3.vater);
		
		Drache d4 = new Drache("d4", datum, d2, d3);
		assertEquals("d4", d4.name);
		assertEquals(datum, d4.geburtsDatum);
		assertNotNull(d4.mutter);
		assertEquals(d2, d4.mutter);
		assertNotNull(d4.vater);
		assertEquals(d3, d4.vater);
		
	}
}
